export {minesweeper};

let minesweeper = ( canvasName, difficulty, tileSize ) => {
    const c = document.querySelector(`#${canvasName}`);
    const context = c.getContext('2d'); 

    let field = ( positionX, positionY, gridName ) => {
        let state = undefined; // undefined === not revealed , 0-8 reveald
        let revealed = false;
        let flagget = false;
        let mine = false;
        let getState = () => state;
        let setMine = () => { mine = true };
        let isMine = () => mine;
        let toggleFlag = () => { 
            flagget = !flagget;
            revealed = !revealed;
        };
        let isFlagget = () => flagget;
        let isRevealed = () => revealed;
        let setReveal = () => { revealed = true; }
        let findMines = () => {
            let mines = 0;
            for (let x = (positionX === 0 ? positionX : (positionX - 1) ) ; x <= ( positionX === (gridName[0].length - 1) ? positionX : (positionX + 1) ) ; x++){
                for (let y = (!positionY ? positionY : (positionY - 1) ) ; y <= ( positionY === (gridName.length  - 1) ? positionY : (positionY + 1) ) ; y++){
                    if ( x !== positionX || y !== positionY ) {
                        if(gridName[y][x].isMine()){ 
                            mines++;
                        }
                    }
                }
            }
            state = mines
            return mines
        }
        return {
            toggleFlag, 
            isFlagget,
            setMine, 
            findMines, 
            isMine,
            getState, 
            isRevealed, 
            setReveal
        }
    } 

// to fix: 
// 1. remove global variebles form board()
    let board = ( difficulty,  ) => {
        // let tileSize = c.width / sizeX
        let mineNum = difficulty === 'easy' ? 10 : difficulty === 'madium' ? 20 : 40
        let sizeX = c.width / tileSize;
        let sizeY = Math.floor(c.height / tileSize)//c.height / tileSize;// Math.floor(c.height / tileSize) 
        let minefield = [];
        const fildSpace = 0;
        let totalToReveal = sizeX * sizeY;
        let minesToFind = mineNum;
        let gameOn = true;
        let moves = 0;
        let flagNum = 0;

        const imgSrc = {
            mine: './../grafic/mine.png',
            flag: './../grafic/flag.jpg'
        }
        

        let clearBoard = () => {
            context.clearRect(0, 0, c.width, c.height);
        }

        // printBoard
        let printBoard = () => {
            clearBoard();
            let fieldX = fildSpace
            let fieldY = fildSpace
            let k = 0
            context.beginPath()
            for( let i = 0 ; i < sizeY ; i++ , fieldY += (tileSize + fildSpace ) ){
                    for( let j = 0 ; j < sizeX + 1  ; j++, fieldX += (tileSize + fildSpace) ){
                    if (j === sizeX ) fieldX = -tileSize
                    context.strokeStyle='#9f9f9f';
                    context.rect(fieldX, fieldY, tileSize, tileSize);
                }
                context.stroke();
            
            }
        }

        let drowImg = (imgSrc, x, y) => {
            const img = new Image();
            img.onload = function() {
                context.drawImage(img, (x * tileSize) + 0.1*tileSize, (y * tileSize) + 0.1*tileSize, tileSize- 0.2*tileSize, tileSize- 0.2*tileSize)
            }
            img.src = imgSrc
        }

        let clearFild = ( reveald, x, y) => {
            if (reveald){
                context.beginPath();
                context.fillStyle = '#dfdfdf';
                context.fillRect((x * tileSize) + 0.1*tileSize, (y * tileSize) + 0.1*tileSize, tileSize- 0.2*tileSize, tileSize- 0.2*tileSize);
            } else {
                context.clearRect((x * tileSize) + 0.1*tileSize, (y * tileSize) + 0.1*tileSize, tileSize- 0.2*tileSize, tileSize- 0.2*tileSize);
            }
        }
        let revealOne = (x, y) => {
            totalToReveal--
            if (totalToReveal === minesToFind && gameOn) { alert('I HAVE ONLY JUST STARTED WINNING')}
            context.font=`${0.7*tileSize}px sans-serif`;
            clearFild(true, x, y)
            context.fillStyle = '#2f2f2f';

            context.fillText(`${minefield[y][x].findMines() === 0 ? '' : minefield[y][x].findMines()}`, (x * tileSize) + 0.3*tileSize, (y * tileSize) + 0.75*tileSize);
            minefield[y][x].setReveal()
        }

        let revealAll = () => {
            for( let i = 0 ; i < sizeX ; i++  ){
                for( let j = 0 ; j < sizeY ; j++ ){  
                    if( minefield[j][i].isMine() ){
                        drowImg(imgSrc.mine, i, j)
                    } else {
                        revealOne(i, j)
                    }
                }
            }    
        }

        let setRundomMines = (notX, notY) => {
            let allPosible = []
            for( let i = 0 ; i < sizeX ; i++  ){
                for( let j = 0 ; j < sizeY ; j++ ){  
                        if( (i !== notX -1) && i !== notX && (i !== notX +1) || j !== notY && (j !== notY +1) && (j !== notY -1)){
                            allPosible.push([i,j])
                        }
                    }
            }
            for ( let m = 0 ; m < mineNum ; m++ ){
                let randIndx = Math.floor(Math.random()*allPosible.length)
                minefield[ allPosible[randIndx][1] ][ allPosible[randIndx][0] ].setMine()
                allPosible.splice(randIndx, 1)
            }
        }

        let settUpGame = () => {
            for( let y = 0 ; y < sizeY ; y++ ){
                minefield[y] = []
                for( let x = 0 ; x < sizeX ; x++ ){
                    minefield[y].push( field(x, y, minefield) )
                }
            }
        }

        let takeAction = (x, y) => {
            let checkNeighbours = (x, y) => {
                for (let xx = (x === 0 ? x : (x - 1) ) ; xx <= ( x === (minefield[0].length - 1) ? x : (x + 1) ) ; xx++){
                    for (let yy = (!y ? y : (y - 1) ) ; yy <= ( y === (minefield.length  - 1) ? y : (y + 1) ) ; yy++){
                        if ( (xx !== x || yy !== y) ) {
                            if( minefield[yy][xx].findMines() > 0  && !minefield[yy][xx].isRevealed() )  {
                                revealOne(xx, yy)
                            } else if ( minefield[yy][xx].findMines() === 0 && !minefield[yy][xx].isRevealed() ) {
                                revealOne(xx, yy) 
                                checkNeighbours(xx, yy)
                            }
                        }
                    }
                }
            }

            if (x <= sizeX && y <= sizeY){
                if ( minefield[y][x].isFlagget() ) return

                if ( moves === 0 ) {
                    minefield = [];
                    settUpGame();
                    printBoard();
                    setRundomMines(x, y);
                } 
            
                if( minefield[y][x].isMine() ){
                    gameOn = false;
                    revealAll()
                    alert('gameOver')
                    return     
                } else if ( ( minefield[y][x].findMines() > 0 && !minefield[y][x].isRevealed()) ) {
                    revealOne(x, y)
                } else if ( ( minefield[y][x].findMines() === 0 && !minefield[y][x].isRevealed() ) ) {
                    revealOne(x,y)
                    checkNeighbours(x, y)
                }
            } 
        }

        let leftClick = e => {
            let xValue = Math.floor(e.offsetX/tileSize);
            let yValue = Math.floor(e.offsetY/tileSize);
            var ctx = e.target.getContext("2d");
            takeAction(xValue, yValue);
            moves++
        }
        let rightClick = e => {
            let xValue = Math.floor(e.offsetX/tileSize);
            let yValue = Math.floor(e.offsetY/tileSize);
            e.preventDefault();
            if (minefield[yValue][xValue].getState() === undefined){
                minefield[yValue][xValue].toggleFlag();
                if(minefield[yValue][xValue].isFlagget()){
                    drowImg(imgSrc.flag, xValue, yValue )
                    flagNum++;
                    if( minefield[yValue][xValue].isMine() ) {
                        minesToFind--
                        totalToReveal--
                        if (minesToFind === 0 && flagNum <= mineNum ) {
                            alert('I HAVE WON A LOT OF TROPHIES')
                        }
                    };
                } else if (!minefield[yValue][xValue].isFlagget()){
                    clearFild(false, xValue, yValue)
                    flagNum--;
                    if( minefield[yValue][xValue].isMine() ) {
                        minesToFind++
                        totalToReveal--
                    }
                }
            } 
            
        }

    let addListeners = () => {
        document.querySelector('#minefield').addEventListener('click', leftClick, false )
        document.querySelector('#minefield').addEventListener('contextmenu', rightClick, false)
    }
    let removeListeners = () => {
        document.querySelector('#minefield').removeEventListener('click', leftClick, false )
        document.querySelector('#minefield').removeEventListener('contextmenu', rightClick, false)
    }

        return {
            printBoard,
            settUpGame,
            addListeners,
            removeListeners,
            reset ()  {
                minefield = []
                moves = 0
            },
            minefield
        }
    }
    return board( difficulty )
}