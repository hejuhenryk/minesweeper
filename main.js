import { display } from './scripts/dispaly.js';
import { minesweeper } from './scripts/minesweeper.js';

let fieldWidth, fieldHeight, tailSize;
let gameDifficulty = 'easy';
let game = {}

const selectors = {
    easy: document.querySelector('.easy'),
    medium: document.querySelector('.medium'),
    hard: document.querySelector('.hard'), 
    minefield: document.querySelector('.minefield_contaier'),
    thisHard: document.querySelector('.thisHard')
}

// add timer 
let timer = display('time_counter', 70, 3);
timer.clearDisplay()
timer.printNum(0)

// add mines counter
let mines = display('mines_counter', 70, 3);
mines.clearDisplay()
mines.printNum(0)


let czasomierz = () => {
    let r = 0;
    setInterval( () => {
        r++;
        timer.clearDisplay()
        timer.printNum(r) 
    }, 1000 )
}
// temp
czasomierz();
let m = 0
document.querySelector('#mines_counter').addEventListener('click', () => {
    m++;
    mines.clearDisplay()
    mines.printNum(m)
} )


let showDifficulty = () => {
    if (gameDifficulty === 'easy' && document.querySelector('.thisHard')) {
        document.querySelector('.thisHard').classList.remove('thisHard')
        selectors.easy.classList.toggle('thisHard')
    } else if (gameDifficulty === 'hard' && document.querySelector('.thisHard')) {
        document.querySelector('.thisHard').classList.remove('thisHard')
        selectors.hard.classList.toggle('thisHard')
    } else if (gameDifficulty === 'medium' && document.querySelector('.thisHard')) {
        document.querySelector('.thisHard').classList.remove('thisHard')
        selectors.medium.classList.toggle('thisHard')
    } else {
        gameDifficulty = 'easy';
        selectors.easy.classList.toggle('thisHard');
    }
}

let changeDifficulty = () => {
    let change = dif => {
        gameDifficulty = dif;
        calcTailSize();
        showDifficulty();
        game = {};
        game = minesweeper( 'minefield', dif, tailSize)
        game.reset();
        game.removeListeners();
        game.printBoard();
        game.settUpGame();
        game.addListeners();

    }
    selectors.easy.addEventListener('click',() => {change('easy')})
    selectors.medium.addEventListener('click', () => {change('medium')})
    selectors.hard.addEventListener('click', () => {change('hard')})
}

let calcTailSize = () => {
    tailSize = gameDifficulty === 'easy' ? fieldWidth/12
        : gameDifficulty === 'medium' ? fieldWidth/18
        : fieldWidth/40;
        console.log(tailSize)

}


let init = () => { 
    // window.removeEventListener('resize', init, false)
    fieldWidth = selectors.minefield.clientWidth;

    changeDifficulty();
    calcTailSize();
    showDifficulty();

    fieldHeight = Math.floor((selectors.minefield.clientHeight * 0.85)/(tailSize ))*(tailSize);
    document.querySelector('#minefield').width = fieldWidth;
    document.querySelector('#minefield').height = fieldHeight;

}

window.onload = () => {
    init();
    // window.addEventListener('resize', init, false)
}

